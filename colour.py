# Copyright 2010 by Andreas Skyman, http://www.dd.chalmers.se/~skymandr/
# You may use and distribute this plug-in under the terms of the GPL.

from pylab import *
import sys

# Colours 0-9 as I see themm, in a format prepared for pylab.imshow():
Colours = transpose(array([
    [[  0,   0,   0]],
    [[255, 255, 255]],
    [[  0,   0, 192]],
    [[192, 000,   0]],
    [[  0, 192,   0]],
    [[192, 192,   0]],
    [[192,  96,   0]],
    [[ 32,   8,   0]],
    [[192,   0, 192]],
    [[192, 192, 192]]
    ]))

# Colours rearanged in a more convenient format for data operations:
C = Colours[:,0,:]

# An approximate colourlist for use with termcolor.colored():
clist = [
       ['grey', ['dark']], ['white', ['bold']],
       ['blue', ['bold']], ['red', None],
       ['green', None], ['yellow', ['bold']],
       ['yellow', None], ['grey', None],
       ['magenta', None], ['white', None]
       ]

def kollaColour(colour):
    """Takes a vector with three values and find the nearest from the 
    matrix _C_. Returns index of nearest colour (which means the 
    colour itself in my world...)
    """

    diffs = ((transpose(C) - colour)**2).sum(axis=1)
    return find(diffs == diffs.min())[0]
  
def makeArray(imfile='stinkbug.png', outfile='out.txt'):
    """Reads an image file and for each pixel looks for the colour in
    _C_ closest to it by calling _kollaColour()_. Returns orignal 
    image as _img_, colour reduced image as _newimg_ and an array of
    colour numbers as numbers. If _outfile_  is specified, writes 
    _numbers_ the that file. 

    Pylab works best with png-images, but others should also work. 
    If the colours of newimg appear odd, it is probably a type error:
    _pylab.imshow()_ works differnetly depending on whether you feed it 
    a matrix of integers or doubles, if coulors are not what you
    expected, try dividing _newimg_ by 255.0 to solve the issue!
    (This happens mostly with gif-images.) For image-formats other 
    than png, both _img_ and _newimg_ may be upside down. This can be
    solved using _pylab.transpose()_ on them.
    """

    img = imread(imfile)
    img = img[:,:,:3]
    newimg  = zeros(img.shape)
    numbers = zeros(img.shape[:2])

    # Fulhack:
    if img.max() > 1:
        dblint = 1
    else:
        dblint = 255.

    for row in range(img.shape[0]):
        for col in range(img.shape[1]):
            numbers[row, col] = kollaColour(img[row, col , :]*dblint)
            newimg[row, col,] = transpose(C[:, numbers[row, col]])/dblint

    if outfile:
        with open(outfile, 'wb') as f:
            for row in range(img.shape[0]):
                for col in range(img.shape[1]):
                    f.write(`int(numbers[row, col])`)
                f.write('\n')

    return img, newimg, numbers

def makeImage(infile='out.txt'):
    """Reads an image in the same format as written by _makeArray()_
    from file _infile_. Returns a list of strings with the rows of
    _infile_ as members for easy printing as _S_, an image array 
    using colours from _C_ as _newimg_ and an array colour numbers
    as _numbers_.
    """

    with open(infile, 'rb') as f:
        S = f.read().split('\n')
    
    newimg  = zeros((len(S)-1, len(S[0]), 3))
    numbers = zeros(newimg.shape[:2])   
   
    for row, line in enumerate(S): 
        if len(line):
            for col, letter in enumerate(line):
                numbers[row, col] = int(letter)
                newimg[row, col,] = transpose(C[:, numbers[row, col]])/255.
  
    return S, newimg, numbers

def printImage(S, on='', bw='no'):
    """Prints image from a string list _S_ of the format returned by
    _makeImage()_ to terminal. If _on_ is specified, uses colour name
    in _on_ as backdrop when printing. If_bw_=='bw' uses black for
    all numbers, if _bw_=='wb', uses white; use _on_='white ' and
    _bw_='bw' for black on white, please note though, that the ANSI
    standard white is not actually white....

    More reasonable would, perhaps, have been to axcept an array of
    the _number_ format, since this is returned by all functions 
    above, but I didn't think that far... Should be easy enough to
    extend it though.

    Requires _termcolor_ module!
    """

    from termcolor import colored

    if len(on):
        on_col = 'on_' + on
    else:
        on_col = None

    for row, line in enumerate(S): 
        for col, letter in enumerate(line):
            if bw.lower() == 'bw':
                 n = 0
            elif bw.lower() == 'wb':
                 n = 1
            else:
                 n = int(letter)
                
            sys.stdout.write(
                colored(letter, clist[n][0], on_color=on_col, 
                    attrs=clist[n][1]))
        sys.stdout.write('\n')
       
