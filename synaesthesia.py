#!/usr/bin/env python

# synaesthesia.py: Gimp plug-in to approximate grapheme-colour synaesthesia.
#     v.2011.02.11.17.64
#     Once installed, you will find it under "Filters/Enhance/Synasthesia..."
#     (It's a bit slow, and undo and cancel doesn't work properly, but that 
#     aside it does the trick...)

# Copyright 2011 by Andreas Skyman, http://www.dd.chalmers.se/~skymandr/
# You may use and distribute this plug-in under the terms of the GPL.

from gimpfu import *
from random import random as rand

# Colours 0-9 approximately as I see them:
Colours = ( (  0,   0,   0),
            (255, 255, 240),
            (  0,   0, 192),
            (192,   0,   0),
            (  0, 192,   0),
            (192, 192,   0),
            (192,  96,   0),
            ( 32,   8,   0),
            (192,   0, 192),
            (192, 192, 192) )


def checkColour(colour):
    """Takes a vector with three values and find the nearest from the 
    matrix _Colour_. Returns index of nearest colour (which means the 
    colour itself in my world...)
    """

    # Find distance in RGB space between sample _colour_ and _Colours_:
    diffs = 10*[0]
    for i, c in enumerate(Colours):
        for j, val in enumerate(c[0:3]):
            diffs[i] += (val - colour[j])**2

    # Find index for the colour from _Colours_ closest to sample _colour_:
    n = diffs.index(min(diffs))

    return Colours[n], n

    
def mylinspace(start, stop, npoints):
    """This function approximates _linspace_ from _pylab_, returning a list
    of _npoints_ numbers evenly spaced between _start_ and _stop_ so that
    the first number in the list is _stant_ and the last is _stop_.
    """

    spaced = range(npoints)
    start, stop, npoints = float(start), float(stop), float(npoints)

    for i, val in enumerate(spaced):
        spaced[i] *= (stop - start)/(npoints - 1)

    return spaced


def synaesthesia(img, drawable, size, font, opacity, howmany, 
                 nx, ny, pickcolour, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9):
    global Colours

    # Initiate progress:
    gimp.progress_init("Synaesthesising " + drawable.name + " ...")
    Colours = (c0, c1, c2, c3, c4, c5, c6, c7, c8, c9)

    # Set up an undo group and disable to save memory:
    pdb.gimp_image_undo_group_start(img)
    pdb.gimp_image_undo_disable(img)

    # Save the current foreground color and make drawable invisible 
    # (invisibility is neccesary to enable an ugly hack, see below):
    pdb.gimp_context_push()
    pdb.gimp_drawable_set_visible(drawable, False)

    # Preamble all done... Here we go:
    w = drawable.width
    h = drawable.height
    if howmany > 0:
        # Create _howmany_ randomly placed numbers:
        for i in range(howmany):
            # Update progress:
            gimp.progress_init("Synaesthesising " + drawable.name + " (" + \
                 `int((100.*i)/howmany)` + "%)")
            # Pick a random pixel and sample colour around it:
            x, y = rand()*w, rand()*h
            try:
                c = pdb.gimp_image_pick_color(img, drawable, x, y, False,
                    True, size*0.5)
            except RuntimeError:
                c = gimp.get_background()
            # Get closest colour and number from synaesthesia array:
            colour, n = checkColour(c[:3])
            if pickcolour == 0: 
                gimp.set_foreground(colour)
            elif pickcolour == 1:
                gimp.set_foreground(c)
            # Print number _n_ at designated coordinates:
            #     NB: Offsets to x and y are sort of font dependent! 
            #         These values work for Monospace Bold...
            new_number = pdb.gimp_text_fontname(img, None, 
                         x-0.25*size, y-0.50*size, 
                         `n`, 0, True, size, PIXELS, font)
            # Set opacity in order for colours to overlap:
            pdb.gimp_layer_set_opacity(new_number, opacity)
    else:
        # Create _nx_*_ny_ numbers in an an orderly fashion:
        colour_string = ""
        for yi, y in enumerate(mylinspace(0, h-0.5*size-1, ny)):
            for xi, x in enumerate(mylinspace(0, w-size*0.5-1, nx)):
                # Update progress:
                gimp.progress_init("Synaesthesising " + drawable.name + " (" + \
                     `int(100.*(xi + yi*nx)/(1.0*(nx*ny)))` + "%)")
                # Sample colour around designated pixel:
                try:
                    c = pdb.gimp_image_pick_color(img, drawable, x, y, False,
                        True, size*0.5)
                except RuntimeError:
                    c = gimp.get_background()
                # Get closest colour and number from synaesthesia array:
                colour, n = checkColour(c[:3])
                colour_string += `n`
                if pickcolour == 0:
                    gimp.set_foreground(colour)
                elif pickcolour == 1: 
                    gimp.set_foreground(c)
                # Print number _n_ at designated coordinates:
                #     NB: Offsets to x and y are sort of font dependent! 
                #         These values work for Monospace Bold...
                new_number = pdb.gimp_text_fontname(img, None, 
                             x-0.25*size, y-0.50*size, 
                             `n`, 0, True, size, PIXELS, font)
                # Set opacity in order for colours to overlap:
                pdb.gimp_layer_set_opacity(new_number, opacity)
            colour_string += '\n'
        # Saves a string of colour numbers to a temporary file:
        try:
            with open('/tmp/synaesthesia_image.txt', 'w') as f:
                f.write(colour_string)
        except IOError:
            pass
    # Merge all number-layers into one (this is the ugly hack...):
    pdb.gimp_image_merge_visible_layers(img, 0)    

    # Restore the old foreground color and make drawable visible 
    # (*cough* ugly hack *cough*):
    pdb.gimp_context_pop()
    pdb.gimp_drawable_set_visible(drawable, True)

    # Reenable undo, then close the undo group:
    pdb.gimp_image_undo_enable(img)
    pdb.gimp_image_undo_group_end(img)


register(
    "python_fu_synaesthesia",
    "Makes you synaesthetic",
    """Approximates grapheme-colour synaesthesia...
    For random sampling, it can be shown that using 
        N ~ 2*width*height/fontsize**2 
    numbers will cover about half the image, assuming fontsize is small compared
    to the image dimensions. Using many more (i.e. more than a factor of 2 more)
    numbers than this will make the image look cluttered. Use a smallish font 
    (~12px) on a high contrast image -- possibly false colour edited -- for best 
    results. (Caution! Using many numbers takes much time; doubling the number 
    of samples more than doubles the run time due to memory constraints!) 

    Using ordered numbers also attempts to save a string of the numbers to:
        /tmp/synaesthesia_image.txt

    Offsets to numbers with respect to the coordinates where the colour is
    sampled are not perfect, so you may have to shift the layer after the
    process is finished.

    To cancel while running, currently the best way is to select one of the
    new layers and delete it, this will crash the plugin.

    Currently, Undo is defunct, so save often!

    On a side note, the default colours approximate the synaesthetic colour 
    perception I've had since childhood.

    Future versions may let you undo and cancel the operation...""",
    "Andreas Skyman",
    "Andreas Skyman",
    "2011",
    "Synaesthesia...",
    "*",      # Alternately use RGB, RGB*, GRAY*, INDEXED etc.
     [
        (PF_IMAGE, "image", "Input image", None),
        (PF_DRAWABLE, "drawable", "Input drawable", None),
        (PF_INT, "size", "Font size:", 16),
        (PF_FONT, "font", "Font face:", "Monospace Bold"),
        (PF_SLIDER, "opacity",  "Opacity:", 80, (0, 100, 1)),
        (PF_INT, "howmany", "Number of numbers (0 for non-random):", 256),
        (PF_INT, "nx", "if 0, number of x-points:", 8),
        (PF_INT, "ny", "... and number of y-points:", 8),
        (PF_OPTION, "pickcolour", "Colour numbers using...", 0, \
        ('synaesthesia palette', 'sampled colour', 'foreground colour')), 
        (PF_COLOUR, "c0", "Colour of number 0:",  (  0,   0,   0)),
        (PF_COLOUR, "c1", "... of number 1:",     (255, 255, 242)),
        (PF_COLOUR, "c2", "... of number 2:",     (  0,   0, 204)),
        (PF_COLOUR, "c3", "... of number 3:",     (204,   0,   0)),
        (PF_COLOUR, "c4", "... of number 4:",     (0,   204,   0)),
        (PF_COLOUR, "c5", "... of number 5:",     (240, 255,  15)),
        (PF_COLOUR, "c6", "... of number 6:",     (204, 102,   0)),
        (PF_COLOUR, "c7", "... of number 7:",     ( 26,  13,   0)),
        (PF_COLOUR, "c8", "... of number 8:",     (204,   0, 204)),
        (PF_COLOUR, "c9", "... and of number 9:", (204, 204, 204)),
    ],
    [],
    synaesthesia, menu="<Image>/Filters/Enhance")

main()
